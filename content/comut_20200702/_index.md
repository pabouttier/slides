+++
title = "Quelques nouvelles de GRICAD"
description = "Quelques infos à propos de GRICAD à destination du COMUT"
outputs = ["Reveal"]
+++

{{% section %}}

## COMUT GRICAD

2 juillet 2020

*Pierre-Antoine Bouttier*

---
### Quelques liens très utiles

* [Le site web de GRICAD](https://gricad.univ-grenoble-alpes.fr/)
* [La nouvelle documentation](https://gricad-doc.univ-grenoble-alpes.fr/)
* [Adresse principale pour le support](mailto:sos-gricad@univ-grenoble-alpes.fr)
* [Pour toute question](gricad-contact@univ-grenoble-alpes.fr)

---
### Table des matières

1. Modèle économique
2. Réponse aux appels à projet CPER et PIA3
3. Focus sur réponse à l'appel équipement G-INP

{{% /section %}}

---

{{% section %}}
## Modèle économique

---
### Groupe de travail

* Myriam Laurens, *responsable financière* 
* Glenn Cougoulat, *adjoint à la direction* 
* Christophe Picard, *MaiMoSiNE et LJK* 
* Gabrielle Feltin, *responsable équipe système* 
* Violaine Louvet, *directrice*

---
### Démarche

![démarche modèle économique](/img/demarche_modele_eco.png)

---
### Où en est-on ?

* Les étapes **1** et **4** OK.
* Les tutelles sont en discussion sur certains points. 
---
À suivre (vous serez informés)
{{% /section %}}

---

{{% section %}}
### Réponses aux appels à projet

* CPER
* PIA3
* Grenoble-INP

---
### CPER - CINAURA

* Réponse coordonnée Auvergne Rhône-Alpes (UC2A, UGA, UdL)
* Demande d'infras d'hébergement
* Demande d'infras calcul et de stockage 

--- 
### PIA3 - MesoNet

* Réponse coordonnées par GENCI pour les mésocentres de calcul
* GRICAD interlocuteur pour la région AURA dans la réponse
* l'accueil d'une des machines de dev/formation + stockage distribué
* 2 postes d'IR prévus qui seront pérennisés (1 à Lyon, 1 à Grenoble)

--- 
### PIA3 - Gaia Data

*cf. Emmanuel Chaljub*

--- 
### PIA3 - TIRREX : 

* Projet de fédération de la communauté recherche en robotique française
* Prestation autour du déploiement d'un entrepôt de données pour la robotique

--- 
### Grenoble-INP

- Appel annuel à équipement GINP
- 40 000€ l'année dernière pour MANTIS2 aujourd'hui en production
- Réponse GRICAD pour 2020 : Espace de stockage haute performance IOPS
{{% /section %}}

---

{{% section %}}
### Espace de stockage haute performance IOPS

- `/bettik` : 1,3Po, pic débit à 12Gb/s
- **TRÈS** performant mais peu adapté aux *nombreux* et *petits* accès
- Perfs globales parfois réduites à cause de cet usage

**Mise en place de `/silenus`**

---
### Caractéristiques prévues de `/silenus`

- BeeGFS (idem que /bettik)
- 1 serveur de méta-données de 4 NVMe de 1,6To
- 4 serveurs de données de 8 SSD de 1,6To
{{% /section %}}

---

{{% section %}}
### Plateforme de supervision et d'indexation de données

**Objectif :** superviser avec précision, côté administrateur et utilisateur l'ensemble des activités sur les infrastructures de calcul et de stockage

---
### Aperçu

![Screenshot d'ELI](/img/eli.png)

---
### Plateforme de supervision et d'indexation de données

- En cours d'installation sur des serveurs recyclés (renforcement de l'infra avec l'offre équipement G-INP)
- D'ores et déjà utilisée par les admins
- A terme disponible pour l'ensemble des utilisateurs
{{% /section %}}

---

{{% section %}}
## Two more things...

---
### Catalogue d'applications
**Objectifs**

* Packager, autant que faire se peut, les logiciels communautaires scientifiques utilisés sur les clusters
* Documenter leurs usages sur nos clusters

**Merci de faire remonter les logiciels que vous utilisez**

---
### Coming soon

![Screenshot perseus-ng](/img/perseus-ng.png)
{{% /section %}}

---

{{% section %}}
{{% slide content="slides.thankyou" %}}
{{% /section %}}